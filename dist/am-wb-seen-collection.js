/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-seen-collection', [ 'am-wb-seen-core' ]);

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('am-wb-seen-collection')
/**
 * 
 */
.controller('AmWbSeenCollectionCollectionCtrl', function($scope, $wbUi) {
	$scope.ctrl = {};
	var ctrl = $scope.ctrl;
	var wbModel = $scope.wbModel;
	var templateJson = null;

	function replaceAll(source, str1, str2, ignore) 
	{
		return source.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,'\\$&'),(ignore?'gi':'g')),(typeof(str2)=='string')?str2.replace(/\$/g,'$$$$"'):str2);
	} 

	/**
	 * Fill templates with document
	 * 
	 * @param document
	 * @returns
	 */
	function compile(document){
		if(!angular.isString(templateJson)){
			templateJson = JSON.stringify(wbModel.template);
		}
		var finalObject = angular.copy(templateJson);
		angular.forEach(wbModel.templateKeys, function(value){
			finalObject = replaceAll(finalObject, '{{'+value+'}}', document[value] || '', false);
		});
		return JSON.parse(finalObject);
	}

	function nextPage(){
		// TODO: maso, 2017: Show progress to loading data
		var documents= [{
			id: 1,
			name: 'first'
		}, {
			id: 2,
			name: 'second'
		}, {
			id: 3,
			name: '3th'
		}];
		angular.forEach(documents, function(document) {
			ctrl.items.push(compile(document));
		});
	}

	/**
	 * Refresh all settings
	 * @returns
	 */
	function refresh() {
		ctrl.items = [];
		templateJson = null;
		return nextPage();
	}

	/**
	 * Open editor dialog
	 * 
	 * Opens editor dialog to edit template.
	 * @returns
	 */
	function editTemplate(){
		return $wbUi.openDialog({
			controller : 'AmWbCollectionTemplateCtrl',
			templateUrl : 'views/am-wb-collection-dialogs/template.html',
			parent : angular.element(document.body),
			clickOutsideToClose : true,
			fullscreen : true,
			locals : {
				wbModel: wbModel,
				style: {}
			}
		})//
		.then(function(template){
			wbModel.template = template;
			refresh();
		});
	}

	/*
	 * Widgets extera actions
	 */
	$scope.extraActions = [{
		icon: 'edit',
		action: editTemplate
	}];
	refresh();
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('am-wb-seen-collection')
/**
 * @ngdoc Controllers
 * @name AmWbSeenCollectionCollection2Ctrl
 * @description List all items of a collection and add into the scope as an item list.
 * 
 * 
 */
.controller('AmWbSeenCollectionCollection2Ctrl', function($scope, $wbUi, $collection, PaginatorParameter) {
	$scope.ctrl = {};
	var ctrl = $scope.ctrl;
	var requests;
	var paginatorParameter = new PaginatorParameter();


	/**
	 * Load next page of documents
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.status === 'working') {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.status = 'working';
		ctrl.collection.documents(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
			ctrl.status = 'relax';
		}, function() {
			ctrl.status = 'fail';
		});
	}
	/**
	 * Refresh all settings
	 * @returns
	 */
	function refresh() {
		ctrl.items = [];
		return nextPage();
	}


	/**
	 * Adding new event
	 * 
	 * @returns
	 */
	function newEvent(){
		return $wbUi.openDialog({
			controller : 'AmWbCollectionModelDialogCtrl',
			templateUrl : 'views/am-wb-collection-dialogs/event.html',
			parent : angular.element(document.body),
			clickOutsideToClose : true,
			fullscreen : true,
			locals : {
				model: {},
				style: {}
			}
		})//
		.then(function(event){
			return ctrl.collection.newDocument(event);
		})//
		.then(function(doc){
			ctrl.items.push(doc);
		});
	}

	/**
	 * Remove event form list and server
	 * @param event
	 * @returns
	 */
	function deleteEvent(event){
		return event.remove()//
		.then(function(){
			var index = ctrl.items.indexOf(event);
			if (index > -1) {
				ctrl.items .splice(index, 1);
			}
		});
	}

	/**
	 * Edit event
	 * 
	 * @param event
	 * @returns
	 */
	function editEvent(event){
		return $wbUi.openDialog({
			controller : 'AmWbCollectionModelDialogCtrl',
			templateUrl : 'views/am-wb-collection-dialogs/event.html',
			parent : angular.element(document.body),
			clickOutsideToClose : true,
			fullscreen : true,
			locals : {
				model: event,
				style: {}
			}
		})
		.then(function(eventNew){
			return eventNew.update();
		});
	}

	/*
	 * Watch collection name change
	 */
	$scope.$watch('wbModel.collection', function(collectionName){
		$collection.collection(collectionName)//
		.then(function(collection){
			ctrl.collection = collection;
			return refresh();
		});
	});

	$scope.newEvent = newEvent;
	$scope.editEvent = editEvent;
	$scope.deleteEvent = deleteEvent;
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/*
 * 
 */
angular.module('am-wb-seen-collection')
/*
 * 
 */
.controller('AmWbCollectionModelDialogCtrl', function($scope, $mdDialog, style, model) {
	$scope.model = model;
	$scope.style = style;
	$scope.hide = function() {
		$mdDialog.hide();
	};
	$scope.cancel = function() {
		$mdDialog.cancel();
	};
	$scope.done = function(a) {
		$mdDialog.hide(a);
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-seen-collection')
/**
 * @ngdoc Controllers
 * @name AmWbCollectionTemplateCtrl
 * @description  Collection Controller 
 */
.controller('AmWbCollectionTemplateCtrl', function($scope, $mdDialog, wbModel, style) {

	function hide() {
		$mdDialog.hide();
	}

	function cancel() {
		$mdDialog.cancel();
	}

	function done(response) {
		$mdDialog.hide(response);
	}

	$scope.template = angular.copy(wbModel.template);
	$scope.style = style;
	$scope.hide = hide;
	$scope.cancel = cancel;
	$scope.done = done;
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-seen-collection')

/**
 * Load widgets
 */
.run(function($settings) {
	$settings.newPage({
		type: 'collection',
		label : 'Collection',
		description : 'Set collection attribute',
		icon : 'settings',
		templateUrl : 'views/am-wb-collection-settings/collection.html'
	});
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-seen-collection')
/**
 * @ngdoc Runs
 * @name Widgets
 * @description Defines and registers some widgets. Following widgets are registerd by this module:
 * 
 * - CollectionDocumentList
 * - CollectionEventList
 * 
 */
.run(function($widget) {
	$widget.newWidget({
		type: 'CollectionDocumentList',
		templateUrl : 'views/am-wb-collection-widgets/document-list.html',
		label : 'Document listt',
		description : 'Lists all document of a collection with a template.',
		image : 'images/wb/html.svg',
		help : 'https://gitlab.com/weburger/am-wb-seen-collection/wikis/home',
		controller: 'AmWbSeenCollectionCollectionCtrl',
		setting:['text' ],
		data:{
			template:{}
		}
	});
	$widget.newWidget({
		type: 'CollectionEventList',
		templateUrl : 'views/am-wb-collection-widgets/event-list.html',
		label : 'Event listt',
		description : 'List of all events.',
		icon : 'events',
		help : 'https://gitlab.com/weburger/am-wb-seen-collection/wikis/home',
		controller: 'AmWbSeenCollectionCollection2Ctrl',
		setting:['text', 'collection'],
		data:{
			template:{}
		}
	});
});

angular.module('am-wb-seen-collection').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/am-wb-collection-dialogs/event.html',
    "<md-dialog aria-label=Event ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <h2 translate>Event</h2> <span flex></span> <md-button class=md-icon-button ng-click=done(model)> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column> <md-input-container class=\"md-icon-float md-block\"> <label>Title</label> <input ng-model=model.title> </md-input-container> <md-input-container class=\"md-icon-float md-block\"> <label>Text</label> <input ng-model=model.text> </md-input-container> <md-input-container class=\"md-icon-float md-block\"> <label>Cover</label> <input ng-model=model.cover> </md-input-container> <md-input-container class=\"md-icon-float md-block\"> <label>Link</label> <input ng-model=model.link> </md-input-container> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/am-wb-collection-dialogs/template.html',
    "<md-dialog aria-label=\"Feature item config\" ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <h2 translate>Template</h2> <span flex></span> <md-button class=md-icon-button ng-click=done(template)> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=row> <wb-setting-panel-group id=directive-collection-template-dialog-setting-panel ng-show=true flex=30 style=\"overflow: auto\"> </wb-setting-panel-group> <wb-content wb-setting-anchor=directive-collection-template-dialog-setting-panel wb-editable=true wb-model=template style=\"overflow: auto\" flex> </wb-content> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/am-wb-collection-settings/action.html',
    " <md-list class=wb-setting-panel> <md-input-container class=\"md-icon-float md-block\"> <label>Label</label> <input ng-model=wbModel.action.label> </md-input-container> <wb-ui-setting-dropdown title=\"Action type\" icon=border_left items=types value=wbModel.action.type> </wb-ui-setting-dropdown>  <md-input-container ng-show=\"wbModel.action.type=='link'\" class=\"md-icon-float md-block\"> <label>Link</label> <input ng-model=wbModel.action.link> </md-input-container> </md-list>"
  );


  $templateCache.put('views/am-wb-collection-settings/collection.html',
    " <md-list class=wb-setting-panel> <md-input-container class=\"md-icon-float md-block\"> <label>Collection</label> <input ng-model=wbModel.collection> </md-input-container> </md-list>"
  );


  $templateCache.put('views/am-wb-collection-settings/features.html',
    "<div class=md-toolbar-tools> <span flex></span> <md-button ng-click=addFeature() class=md-icon-button aria-label=\"Add featrue\"> <wb-icon>add</wb-icon> </md-button> <md-button ng-click=removeAllFeatures() class=md-icon-button aria-label=\"Add featrue\"> <wb-icon>clear</wb-icon> </md-button> </div> <md-list> <md-list-item ng-repeat=\"feature in wbModel.features\" ng-click=\"editFeature(feature, $event)\" class=noright> <wb-icon>link</wb-icon> <p>{{ feature.title }}</p>  <wb-icon ng-show=\"$index<wbModel.features.length-1\" ng-click=\"moveDownFeature(feature, $event)\" class=\"md-secondary md-hue-3\" aria-label=\"Move up\">wb-common-movedown <wb-icon ng-show=\"$index>0\" ng-click=\"moveUpFeature(feature, $event)\" class=\"md-secondary md-hue-3\" aria-label=\"Move up\">wb-common-moveup <wb-icon class=md-secondary ng-click=\"removeFeature(feature, $event)\" aria-label=Chat>delete </wb-icon></wb-icon></wb-icon></md-list-item> </md-list>"
  );


  $templateCache.put('views/am-wb-collection-widgets/document-list.html',
    "<div layout=column> <wb-content ng-repeat=\"model in ctrl.items\" wb-editable=false wb-model=model style=\"overflow: auto\"> </wb-content> </div>"
  );


  $templateCache.put('views/am-wb-collection-widgets/event-list.html',
    " <div layout=row layout-wrap layout-padding> <div layout=row flex=100 flex-gt-md=50 ng-repeat=\"tile in ctrl.items\"> <div layout=column> <div ng-if=wbEditable> <md-button class=md-icon-button ng-click=editEvent(tile)> <wb-icon>edit</wb-icon> </md-button> <md-button class=md-icon-button ng-click=deleteEvent(tile)> <wb-icon>delete</wb-icon> </md-button> </div> <img width=128px height=128px alt={{tile.title}} ng-src=\"{{tile.cover}}\"> </div> <div layout=column layout-align=\"center start\" flex> <h3><a href={{tile.link}}>{{tile.title}}</a></h3> <div ng-bind-html=\"tile.text | wbunsafe\"> </div> </div> </div> <md-button flex=100 flex-gt-md=50 ng-if=wbEditable aria-label=\"add social\" ng-click=newEvent()> <wb-icon>add</wb-icon> </md-button> </div>"
  );

}]);
