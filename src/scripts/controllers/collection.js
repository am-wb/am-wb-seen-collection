/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('am-wb-seen-collection')
/**
 * 
 */
.controller('AmWbSeenCollectionCollectionCtrl', function($scope, $wbUi) {
	$scope.ctrl = {};
	var ctrl = $scope.ctrl;
	var wbModel = $scope.wbModel;
	var templateJson = null;

	function replaceAll(source, str1, str2, ignore) 
	{
		return source.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,'\\$&'),(ignore?'gi':'g')),(typeof(str2)=='string')?str2.replace(/\$/g,'$$$$"'):str2);
	} 

	/**
	 * Fill templates with document
	 * 
	 * @param document
	 * @returns
	 */
	function compile(document){
		if(!angular.isString(templateJson)){
			templateJson = JSON.stringify(wbModel.template);
		}
		var finalObject = angular.copy(templateJson);
		angular.forEach(wbModel.templateKeys, function(value){
			finalObject = replaceAll(finalObject, '{{'+value+'}}', document[value] || '', false);
		});
		return JSON.parse(finalObject);
	}

	function nextPage(){
		// TODO: maso, 2017: Show progress to loading data
		var documents= [{
			id: 1,
			name: 'first'
		}, {
			id: 2,
			name: 'second'
		}, {
			id: 3,
			name: '3th'
		}];
		angular.forEach(documents, function(document) {
			ctrl.items.push(compile(document));
		});
	}

	/**
	 * Refresh all settings
	 * @returns
	 */
	function refresh() {
		ctrl.items = [];
		templateJson = null;
		return nextPage();
	}

	/**
	 * Open editor dialog
	 * 
	 * Opens editor dialog to edit template.
	 * @returns
	 */
	function editTemplate(){
		return $wbUi.openDialog({
			controller : 'AmWbCollectionTemplateCtrl',
			templateUrl : 'views/am-wb-collection-dialogs/template.html',
			parent : angular.element(document.body),
			clickOutsideToClose : true,
			fullscreen : true,
			locals : {
				wbModel: wbModel,
				style: {}
			}
		})//
		.then(function(template){
			wbModel.template = template;
			refresh();
		});
	}

	/*
	 * Widgets extera actions
	 */
	$scope.extraActions = [{
		icon: 'edit',
		action: editTemplate
	}];
	refresh();
});